#!/bin/sh

PORT=${PORT:-443}

# build xen-orchestra from sources
cd build/ || exit
git clone https://github.com/ronivay/xen-orchestra-docker
sudo docker build -t xen-orchestra --ulimit nofile=100000:100000 xen-orchestra-docker
sudo docker build -t xen-orchestra-https .
cd "$OLDPWD" || exit

# create mounted folders first to gain permissions
mkdir -p mount/xo-server
mkdir -p mount/redis
mkdir -p mount/ssl

# generate certificate
if [ ! -f "mount/ssl/key.pem" ] || [ ! -f "mount/ssl/certificate.pem" ]; then
    sudo rm -f mount/ssl/*
    sudo openssl req -x509 \
        -nodes -days 3650 -newkey rsa:4096 \
        -subj "/C=/ST=/L=/O=/CN=" \
        -keyout mount/ssl/key.pem \
        -out mount/ssl/certificate.pem
fi

# run XO with https
sudo docker rm -f xo
sudo docker run -itd --rm \
    --privileged \
    -p "$PORT":443 \
    -v "$(pwd)"/mount/xo-server:/var/lib/xo-server:z \
    -v "$(pwd)"/mount/redis:/var/lib/redis:z \
    -v "$(pwd)"/mount/ssl:/ssl:z \
    --name xo \
    xen-orchestra-https
